#include <stdio.h>
int main(int argc, char const *argv[]) {

  int i=0;
  int j=0;
  int columnas=0;
  int renglones=0;

  printf("Escribe el número de columnas: ");
  scanf("%d", &columnas);
  printf("Escribe el número de renglones: ");
  scanf("%d", &renglones);
  
  for ( i = 0; i < renglones; i++) {
    for (j = 0; j < columnas; j++) {
      printf("+");
    }
    printf("\n");
  }
  return 0;
}
